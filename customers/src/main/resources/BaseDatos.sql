-- Creación de la tabla UserEntity
-- CREATE TABLE usuario (
--     id UUID PRIMARY KEY,
--     name VARCHAR(255),
--     email VARCHAR(255) UNIQUE NOT NULL,
--     password VARCHAR(255),
--     created TIMESTAMP,
--     modified TIMESTAMP,
--     last_login TIMESTAMP,
--     token VARCHAR(255),
--     is_active BOOLEAN
-- );

-- Creación de la tabla PhoneEntity
-- CREATE TABLE telefonos (
--     id SERIAL PRIMARY KEY,
--     number VARCHAR(255),
--     citycode VARCHAR(255),
--     countrycode VARCHAR(255),
--     user_id UUID REFERENCES usuario(id)
-- );

INSERT INTO customer (identification, name, gender, age, address, phone) VALUES
('123456789', 'Juan', 'Male', '30', '123 Main St', '555-1234');
INSERT INTO customer (identification, name, gender, age, address, phone) VALUES
('678123459', 'Pedro', 'Male', '28', '123 Main St', '555-1234');
INSERT INTO customer (identification, name, gender, age, address, phone) VALUES
('345126789', 'Tomas', 'Male', '25', '123 Main St', '555-1234');
INSERT INTO customer (identification, name, gender, age, address, phone) VALUES
('561472389', 'Horacio', 'Male', '42', '123 Main St', '555-1234');
INSERT INTO customer (identification, name, gender, age, address, phone) VALUES
('891253467', 'Alberto', 'Male', '30', '123 Main St', '555-1234');

