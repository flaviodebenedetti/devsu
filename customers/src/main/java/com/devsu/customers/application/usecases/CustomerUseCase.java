package com.devsu.customers.application.usecases;

import com.devsu.customers.domain.dto.CustomerDto;
import com.devsu.customers.domain.request.CustomerCreateRequest;
import com.devsu.customers.domain.request.CustomerRequest;
import com.devsu.customers.domain.response.CustomerResponse;

import java.util.List;
import java.util.Optional;

public interface CustomerUseCase {
    List<CustomerDto> findAll();
    CustomerResponse create(CustomerCreateRequest request);
    CustomerResponse update(CustomerRequest request);
    CustomerResponse patch(CustomerRequest request);
    CustomerResponse delete(String id);
}
