package com.devsu.customers.application.service;

import com.devsu.customers.application.usecases.CustomerUseCase;
import com.devsu.customers.domain.dto.CustomerDto;
import com.devsu.customers.domain.entities.CustomerEntity;
import com.devsu.customers.domain.request.CustomerCreateRequest;
import com.devsu.customers.domain.request.CustomerRequest;
import com.devsu.customers.domain.response.CustomerResponse;
import com.devsu.customers.infrastructure.exceptions.CustomerNotExistsException;
import com.devsu.customers.infrastructure.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Component
@Slf4j
public class CustomerService implements CustomerUseCase {

    private final CustomerRepository repository;

    @Override
    public List<CustomerDto> findAll() {
        List listAccounts = repository.findAll();
        List<CustomerDto> listDto = new ArrayList<>();
        for (Object account : listAccounts) {
            CustomerEntity customerEntity = (CustomerEntity) account;
            listDto.add(customerEntity.toAccountDto());
        }
        return listDto;
    }

    @Override
    public CustomerResponse create(CustomerCreateRequest request) {
        CustomerEntity customerEntity = createCustomer(request);
        repository.save(customerEntity);
        return CustomerResponse.buildUpdateOkResponse();
    }

    @Override
    public CustomerResponse update(CustomerRequest request) {
        Optional optional = repository.findById(String.valueOf(request.id()));
        if(optional.isEmpty()){
            throw new CustomerNotExistsException("El ID " + request.id() + " no está registrado");
        }
        CustomerEntity customerEntity = updateCustomer(request);
        customerEntity.setClientId(request.id());
        repository.save(customerEntity);
        return CustomerResponse.buildUpdateOkResponse();
    }

    @Override
    public CustomerResponse patch(CustomerRequest request) {
        Optional optional = repository.findById(String.valueOf(request.id()));
        if(optional.isEmpty()){
            throw new CustomerNotExistsException("El ID " + request.id() + " no está registrado");
        }
        CustomerEntity customerEntity = updateToPatch(request);
        customerEntity.setClientId(request.id());
        repository.save(customerEntity);
        return CustomerResponse.buildUpdateOkResponse();
    }

    @Override
    public CustomerResponse delete(String id) {
        Optional optional = repository.findById(String.valueOf(id));
        if(optional.isEmpty()){
            throw new CustomerNotExistsException("El ID " + id + " no está registrado");
        }
        repository.deleteById(id);
        return CustomerResponse.buildDeleteOkResponse();
    }

    private CustomerEntity createCustomer(CustomerCreateRequest request) {
        CustomerEntity customerEntityCreate = new CustomerEntity();
        customerEntityCreate.setPassword(request.password());
        customerEntityCreate.setState(request.state());
        customerEntityCreate.setName(request.name());
        customerEntityCreate.setAge(request.age());
        customerEntityCreate.setGender(request.gender());
        customerEntityCreate.setPhone(request.phone());
        customerEntityCreate.setAddress(request.address());
        customerEntityCreate.setIdentification(request.identification());
        return customerEntityCreate;
    }

    private CustomerEntity updateCustomer(CustomerRequest request) {
        CustomerEntity customerEntityToUpdate = new CustomerEntity();
        customerEntityToUpdate.setName(request.name());
        customerEntityToUpdate.setPassword(request.password());
        customerEntityToUpdate.setState(request.state());
        customerEntityToUpdate.setAge(request.age());
        customerEntityToUpdate.setAddress(request.address());
        customerEntityToUpdate.setGender(request.gender());
        customerEntityToUpdate.setPhone(request.phone());
        return customerEntityToUpdate;
    }

    private CustomerEntity updateToPatch(CustomerRequest request) {
        CustomerEntity customerEntityToUpdate = new CustomerEntity();

        if(request.name() != null && !request.name().isEmpty()){
            customerEntityToUpdate.setName(request.name());
        }
        if(request.password() != null && !request.password().isEmpty()){
            customerEntityToUpdate.setPassword(request.password());
        }
        if(request.state() != null){
            customerEntityToUpdate.setState(request.state());
        }
        if(request.age() != null && !request.age().isEmpty()){
            customerEntityToUpdate.setAge(request.age());
        }
        if(request.address() != null && !request.address().isEmpty()){
            customerEntityToUpdate.setAddress(request.address());
        }
        if(request.gender() != null && !request.gender().isEmpty()){
            customerEntityToUpdate.setGender(request.gender());
        }
        if(request.phone() != null && !request.phone().isEmpty()){
            customerEntityToUpdate.setPhone(request.phone());
        }

        return customerEntityToUpdate;
    }
}
