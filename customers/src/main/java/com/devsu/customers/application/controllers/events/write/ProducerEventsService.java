package com.devsu.customers.application.controllers.events.write;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Service
@Slf4j
public class ProducerEventsService {

    private final KafkaTemplate<String, Map> producer;

    @Value("${spring.kafka.producer.topic.name}")
    private String accountTopicName;

    @Retryable(value = {RuntimeException.class}, maxAttempts = 3, backoff = @Backoff(1000))
    public void publish() {

        Map mapData = new HashMap();
        mapData.put("", "");

        log.info("Send message");

        this.producer.send(accountTopicName, mapData);
    }
}
