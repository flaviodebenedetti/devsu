package com.devsu.customers.application.controllers;

import com.devsu.customers.application.usecases.CustomerUseCase;
import com.devsu.customers.domain.request.CustomerCreateRequest;
import com.devsu.customers.domain.request.CustomerRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clientes")
@RequiredArgsConstructor
@Slf4j
public class CustomerController {
    private final CustomerUseCase customerUseCase;

    @GetMapping()
    public ResponseEntity getCustomers(){
        return ResponseEntity.ok(customerUseCase.findAll());
    }

    @PostMapping
    public ResponseEntity createCustomer(@RequestBody CustomerCreateRequest createRequest){
        return ResponseEntity.ok(customerUseCase.create(createRequest));
    }

    @PutMapping
    public ResponseEntity updateCustomer(@RequestBody CustomerRequest customerRequest){
        return ResponseEntity.ok(customerUseCase.update(customerRequest));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteCustomer(@PathVariable String id){
        return ResponseEntity.ok(customerUseCase.delete(id));
    }

    @PatchMapping
    public ResponseEntity patchCustomer(@RequestBody CustomerRequest customerRequest){
        return ResponseEntity.ok(customerUseCase.patch(customerRequest));
    }
}
