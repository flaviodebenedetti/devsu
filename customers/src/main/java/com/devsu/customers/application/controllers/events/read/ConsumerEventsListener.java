package com.devsu.customers.application.controllers.events.read;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Map;

@RequiredArgsConstructor
@Service
@Slf4j
public class ConsumerEventsListener {

    @KafkaListener(
            topics = "${spring.kafka.consumer.topic.name}",
            groupId = "${spring.kafka.consumer.group-id}",
            autoStartup = "true"
    )
    public void listener(Map<String, String> message){
        log.info("Received message");

        log.info(message.get("clientId"));
        log.info(message.get("dateFrom"));
        log.info(message.get("dateEnd"));

        log.info("Save message listener");
    }
}
