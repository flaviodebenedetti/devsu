package com.devsu.customers.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerDto {

    private Long clientId;
    private Boolean state;
    private String identification;
    private String nombre;
    private String genero;
    private String edad;
    private String direccion;
    private String telefono;
}
