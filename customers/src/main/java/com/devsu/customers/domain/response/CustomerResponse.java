package com.devsu.customers.domain.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerResponse {
    private int statusCode;
    private HttpStatus status;
    private String description;

    public static CustomerResponse buildOkResponse() {
        return CustomerResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    public static CustomerResponse buildUpdateOkResponse() {
        return CustomerResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .build();
    }

    public static CustomerResponse buildDeleteOkResponse() {
        return CustomerResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .description("Registro eliminado con éxito")
                .build();
    }
}
