package com.devsu.customers.domain.entities;

import jakarta.persistence.MappedSuperclass;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
public class PersonEntity {

    private String identification;
    private String name;
    private String gender;
    private String age;
    private String address;
    private String phone;
}
