package com.devsu.customers.domain.entities;

import com.devsu.customers.domain.dto.CustomerDto;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "customer")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CustomerEntity extends PersonEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long clientId;
    private String password;
    private Boolean state;

    public CustomerDto toAccountDto() {
        return CustomerDto.builder()
                .clientId(clientId)
                .identification(getIdentification())
                .nombre(getName())
                .edad(getAge())
                .state(state)
                .direccion(getAddress())
                .telefono(getPhone())
                .genero(getGender())
                .build();
    }
}
