package com.devsu.customers.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public record CustomerRequest (
        @JsonProperty("id") Long id,
        @JsonProperty("password") String password,
        @JsonProperty("state") Boolean state,
        @JsonProperty("identification") String identification,
        @JsonProperty("name") String name,
        @JsonProperty("age") String age,
        @JsonProperty("address") String address,
        @JsonProperty("gender") String gender,
        @JsonProperty("phone") String phone
){}
