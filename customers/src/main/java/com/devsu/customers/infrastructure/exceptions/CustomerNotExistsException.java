package com.devsu.customers.infrastructure.exceptions;


public class CustomerNotExistsException extends RuntimeException {

    public CustomerNotExistsException(String message) {
        super(message);
    }

}
