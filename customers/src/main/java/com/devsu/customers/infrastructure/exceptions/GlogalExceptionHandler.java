package com.devsu.customers.infrastructure.exceptions;

import com.devsu.customers.domain.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlogalExceptionHandler {

    @ExceptionHandler(CustomerNotExistsException.class)
    public ResponseEntity<ErrorResponse> handlerCustomerNotExistsException(CustomerNotExistsException exception){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}
