package com.devsu.accounts.infrastructure.repository;

import com.devsu.accounts.domain.entities.MovementEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovementRepository extends JpaRepository<MovementEntity, String> {

    @Query(value = "SELECT * FROM movement WHERE account.accountNumber = :accountNumber", nativeQuery = true)
    List<MovementEntity> findMovementByAccountNumber(@Param("accountNumber") Long accountNumber);

    List<MovementEntity> findByAccount_AccountNumber(Long accountNumber);
}
