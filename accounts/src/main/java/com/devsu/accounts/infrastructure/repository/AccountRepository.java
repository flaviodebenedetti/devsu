package com.devsu.accounts.infrastructure.repository;

import com.devsu.accounts.domain.entities.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, String> {

    @Query(value = "SELECT * FROM account WHERE account_number = :accountNumber", nativeQuery = true)
    AccountEntity findByAccountNumber(@Param("accountNumber") Long accountNumber);

    /*@Query(value = "SELECT * FROM account WHERE account_number = :accountNumber", nativeQuery = true)
    AccountEntity findAccountWithMovementByDate(@Param("accountNumber") Long accountNumber, Date desde, Date hasta);*/
}
