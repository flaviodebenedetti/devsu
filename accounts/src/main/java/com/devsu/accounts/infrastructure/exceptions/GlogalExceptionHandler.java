package com.devsu.accounts.infrastructure.exceptions;

import com.devsu.accounts.domain.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

@RestControllerAdvice
public class GlogalExceptionHandler {

    @ExceptionHandler(NotAvailableBalanceException.class)
    public ResponseEntity<ErrorResponse> handlerNotAvailableBalanceException(NotAvailableBalanceException exception){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AccountNotExistsException.class)
    public ResponseEntity<ErrorResponse> handlerAccountNotExistsException(AccountNotExistsException exception){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MovementNotExistsException.class)
    public ResponseEntity<ErrorResponse> handlerMovementNotExistsException(MovementNotExistsException exception){
        ErrorResponse errorResponse = ErrorResponse.builder().mensaje(exception.getMessage()).build();
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }
}
