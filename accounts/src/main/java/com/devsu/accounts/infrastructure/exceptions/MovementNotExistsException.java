package com.devsu.accounts.infrastructure.exceptions;

public class MovementNotExistsException extends RuntimeException {

    public MovementNotExistsException(String message) {
        super(message);
    }

}