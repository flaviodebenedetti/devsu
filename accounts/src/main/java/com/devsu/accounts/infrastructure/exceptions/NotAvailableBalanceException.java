package com.devsu.accounts.infrastructure.exceptions;


public class NotAvailableBalanceException extends RuntimeException {

    public NotAvailableBalanceException(String message) {
        super(message);
    }

}
