package com.devsu.accounts.infrastructure.exceptions;

public class AccountNotExistsException extends RuntimeException {

    public AccountNotExistsException(String message) {
        super(message);
    }

}
