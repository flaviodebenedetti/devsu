package com.devsu.accounts.application.controllers;

import com.devsu.accounts.application.usecases.AccountUseCase;
import com.devsu.accounts.domain.dto.AccountDto;
import com.devsu.accounts.domain.request.AccountRequest;
import com.devsu.accounts.domain.request.MovementRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/cuentas")
@RequiredArgsConstructor
@Slf4j
public class AccountsController {

    private final AccountUseCase accountUseCase;

    @GetMapping()
    public ResponseEntity<List<AccountDto>> getAccounts(){
        return ResponseEntity.ok(accountUseCase.findAll());
    }

    @GetMapping("/reportes/{clientId}")
    public ResponseEntity getReport(@PathVariable String clientId, @RequestParam String desde, @RequestParam String hasta){
        return ResponseEntity.ok(accountUseCase.report(clientId, desde, hasta));
    }

    @PostMapping
    public ResponseEntity createAccount(@RequestBody AccountRequest accountRequest){
        return ResponseEntity.ok(accountUseCase.create(accountRequest));
    }

    @PutMapping
    public ResponseEntity updateAccount(@RequestBody AccountRequest accountRequest){
        return ResponseEntity.ok(accountUseCase.update(accountRequest));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteAccount(@PathVariable String id){
        return ResponseEntity.ok(accountUseCase.delete(id));
    }

    @PatchMapping
    public ResponseEntity patchAccount(@RequestBody AccountRequest accountRequestt){
        return ResponseEntity.ok(accountUseCase.patch(accountRequestt));
    }
}
