package com.devsu.accounts.application.controllers.events.read;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import java.util.Map;

@RequiredArgsConstructor
@Service
@Slf4j
public class ConsumerEventsListener {

    @KafkaListener(
            topics = "${spring.kafka.consumer.topic.name}",
            groupId = "${spring.kafka.consumer.group-id}",
            autoStartup = "true"
    )
    public void listener(Map<String, Long> message){
        log.info("Received message");

        log.info(message.get("accountNumber").toString());
        log.info(message.get("customerId").toString());

        log.info("Save message listener");
    }
}
