package com.devsu.accounts.application.controllers;

import com.devsu.accounts.application.usecases.MovementUseCase;
import com.devsu.accounts.domain.request.MovementCreateRequest;
import com.devsu.accounts.domain.request.MovementRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/movimientos")
@RequiredArgsConstructor
@Slf4j
public class MovementsController {

    private final MovementUseCase movementUseCase;

    @GetMapping()
    public ResponseEntity getMovements(){
        return ResponseEntity.ok(movementUseCase.findAll());
    }

    @PostMapping
    public ResponseEntity createMovement(@RequestBody MovementCreateRequest movementCreateRequest){
        return ResponseEntity.ok(movementUseCase.create(movementCreateRequest));
    }

    @PutMapping
    public ResponseEntity updateMovement(@RequestBody MovementRequest movementRequest){
        return ResponseEntity.ok(movementUseCase.update(movementRequest));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteMovement(@PathVariable String id){
        return ResponseEntity.ok(movementUseCase.delete(id));
    }

    @PatchMapping
    public ResponseEntity patchMovement(@RequestBody MovementRequest movementRequest){
        return ResponseEntity.ok(movementUseCase.patch(movementRequest));
    }
}
