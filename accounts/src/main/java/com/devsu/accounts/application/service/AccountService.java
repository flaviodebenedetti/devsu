package com.devsu.accounts.application.service;

import com.devsu.accounts.application.controllers.events.write.ProducerEventsService;
import com.devsu.accounts.application.usecases.AccountUseCase;
import com.devsu.accounts.domain.dto.AccountDto;
import com.devsu.accounts.domain.entities.AccountEntity;
import com.devsu.accounts.domain.entities.MovementEntity;
import com.devsu.accounts.domain.enums.AccountType;
import com.devsu.accounts.domain.request.AccountRequest;
import com.devsu.accounts.domain.response.AccountResponse;
import com.devsu.accounts.infrastructure.exceptions.AccountNotExistsException;
import com.devsu.accounts.infrastructure.repository.AccountRepository;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Component
@Slf4j
public class AccountService implements AccountUseCase {

    private final AccountRepository repository;
    private final ProducerEventsService producerEventsService;

    @Override
    public List<AccountDto> findAll() {
        List listAccounts = repository.findAll();
        List<AccountDto> listDto = new ArrayList<>();
        for (Object account : listAccounts) {
            AccountEntity accountEntity = (AccountEntity) account;
            listDto.add(accountEntity.toAccountDto());
        }
        return listDto;
    }

    @Override
    public Optional<AccountDto> create(AccountRequest request) {
        AccountEntity accountEntity = createAccountEntity(request);
        repository.save(accountEntity);
        return Optional.of(accountEntity.toAccountDto());
    }

    @Override
    public AccountResponse update(AccountRequest request) {
        Optional optional = repository.findById(String.valueOf(request.accountNumber()));
        if(optional.isEmpty()){
            throw new AccountNotExistsException("La cuenta con numero " + request.accountNumber() + " no existe");
        }
        AccountEntity accountEntity = updateAccount(request);
        accountEntity.setAccountNumber(request.accountNumber());
        accountEntity.setListMovements(getMovementsFromDB(optional));
        repository.save(accountEntity);
        return AccountResponse.buildUpdateOkResponse();
    }

    private List<MovementEntity> getMovementsFromDB(Optional optional) {
        AccountEntity account = (AccountEntity) optional.get();
        return account.getListMovements();
    }

    @Override
    public AccountResponse patch(AccountRequest request) {
        Optional optional = repository.findById(String.valueOf(request.accountNumber()));
        if(optional.isEmpty()){
            throw new AccountNotExistsException("La cuenta con numero " + request.accountNumber() + " no existe");
        }
        AccountEntity accountEntity = updateToPatch(request);
        accountEntity.setAccountNumber(request.accountNumber());
        repository.save(accountEntity);
        return AccountResponse.buildUpdateOkResponse();
    }

    @Override
    public AccountResponse delete(String id) {
        Optional optional = repository.findById(id);
        if(optional.isEmpty()){
            throw new AccountNotExistsException("La cuenta con numero " + id + " no existe");
        }
        repository.deleteById(id);
        return AccountResponse.buildDeleteOkResponse();
    }

    @Override
    public AccountResponse report(String id, String desde, String hasta) {

        producerEventsService.publish(id, desde, hasta);

        return AccountResponse.buildMessagePublicateResponse();
    }

    private AccountEntity createAccountEntity(AccountRequest request) {
        AccountEntity accountEntityCreate = new AccountEntity();
        accountEntityCreate.setAccountType(AccountType.fromCode(request.accountType()));
        accountEntityCreate.setOpeningBalance(request.openingBalance());
        accountEntityCreate.setState(request.state());
        return accountEntityCreate;
    }

    private AccountEntity updateAccount(AccountRequest request) {
        AccountEntity accountEntityToUpdate = new AccountEntity();
        accountEntityToUpdate.setAccountType(AccountType.fromCode(request.accountType()));
        accountEntityToUpdate.setOpeningBalance(request.openingBalance());
        accountEntityToUpdate.setState(request.state());
        return accountEntityToUpdate;
    }

    private AccountEntity updateToPatch(AccountRequest request) {
        AccountEntity accountEntityToUpdate = new AccountEntity();

        if(request.accountType() != null && !request.accountType().isEmpty()){
            accountEntityToUpdate.setAccountType(AccountType.fromCode(request.accountType()));
        }
        if(request.openingBalance() != null){
            accountEntityToUpdate.setOpeningBalance(request.openingBalance());
        }
        if(request.state() != null){
            accountEntityToUpdate.setState(request.state());
        }

        return accountEntityToUpdate;
    }
}
