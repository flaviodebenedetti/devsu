package com.devsu.accounts.application.usecases;

import com.devsu.accounts.domain.dto.MovementDto;
import com.devsu.accounts.domain.request.MovementCreateRequest;
import com.devsu.accounts.domain.request.MovementRequest;
import com.devsu.accounts.domain.response.MovementResponse;

import java.util.List;
import java.util.Optional;

public interface MovementUseCase {

    List<MovementDto> findAll();
    MovementResponse create(MovementCreateRequest request);
    MovementResponse update(MovementRequest request);
    MovementResponse patch(MovementRequest request);
    MovementResponse delete(String id);
}
