package com.devsu.accounts.application.service;

import com.devsu.accounts.application.usecases.MovementUseCase;
import com.devsu.accounts.domain.dto.MovementDto;
import com.devsu.accounts.domain.entities.AccountEntity;
import com.devsu.accounts.domain.entities.MovementEntity;
import com.devsu.accounts.domain.enums.MovementType;
import com.devsu.accounts.domain.request.MovementCreateRequest;
import com.devsu.accounts.domain.request.MovementRequest;
import com.devsu.accounts.domain.response.MovementResponse;
import com.devsu.accounts.infrastructure.exceptions.MovementNotExistsException;
import com.devsu.accounts.infrastructure.exceptions.NotAvailableBalanceException;
import com.devsu.accounts.infrastructure.repository.AccountRepository;
import com.devsu.accounts.infrastructure.repository.MovementRepository;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

@RequiredArgsConstructor
@Component
@Slf4j
public class MovementService implements MovementUseCase {

    private final MovementRepository movementRepository;
    private final AccountRepository accountRepository;

    @Override
    public List<MovementDto> findAll() {
        List listMovement = movementRepository.findAll();
        List<MovementDto> listDto = new ArrayList<>();

        for (Object movement : listMovement) {
            MovementEntity movementEntity = (MovementEntity) movement;
            listDto.add(movementEntity.toMovementDto());
        }

        return listDto;
    }

    @Override
    public MovementResponse create(MovementCreateRequest request) {
        MovementEntity movementEntity = createMovementEntity(request);
        AccountEntity account = accountRepository.findByAccountNumber(request.accountNumber());

        if(!isAvailableBalance(movementEntity.getAmount(), account.getOpeningBalance())){
            throw new NotAvailableBalanceException("Saldo no disponible");
        }

        updateBalanceInAssociatedAccount(account, movementEntity.getAmount());
        movementEntity.setAccount(account);
        account.getListMovements().add(movementEntity);
        accountRepository.save(account);

        return MovementResponse.buildCreatedResponse();
    }

    @Override
    public MovementResponse update(MovementRequest request) {
        Optional optional = movementRepository.findById(request.id());
        if(optional.isEmpty()){
            throw new MovementNotExistsException("No se encontró el ID a actualizar");
        }

        MovementEntity movementEntity = updateMovement(request);
        movementEntity.setId(request.id());
        movementRepository.save(movementEntity);

        return MovementResponse.buildUpdateOkResponse();
    }

    @Override
    public MovementResponse patch(MovementRequest request) {
        Optional optional = movementRepository.findById(request.id());
        if(optional.isEmpty()){
            throw new MovementNotExistsException("No se encontró el ID a actualizar");
        }

        MovementEntity movementEntity = updateToPatch(request);
        movementEntity.setId(request.id());
        movementRepository.save(movementEntity);

        return MovementResponse.buildOkResponse();
    }

    @Override
    public MovementResponse delete(String id) {
        Optional optional = movementRepository.findById(id);
        if(optional.isEmpty()){
            throw new MovementNotExistsException("No se encontró el ID a actualizar");
        }
        movementRepository.deleteById(id);
        return MovementResponse.buildDeleteOkResponse();
    }

    private MovementEntity createMovementEntity(MovementCreateRequest request) {
        MovementEntity movementEntityCreate = new MovementEntity();
        movementEntityCreate.setMovementType(MovementType.fromCode(request.type()));
        movementEntityCreate.setDate(formatDate(request.date()));
        movementEntityCreate.setAmount(request.amount());
        movementEntityCreate.setAccount(AccountEntity.builder().accountNumber(request.accountNumber()).build());
        return movementEntityCreate;
    }

    private void updateBalanceInAssociatedAccount(AccountEntity account, Double value) {
        Double updatedBalance = account.getOpeningBalance() + value;
        account.setOpeningBalance(updatedBalance);
    }

    private MovementEntity updateMovement(MovementRequest request) {
        MovementEntity movementEntityUpdate = new MovementEntity();
        movementEntityUpdate.setMovementType(MovementType.fromCode(request.type()));
        movementEntityUpdate.setDate(formatDate(request.date()));
        movementEntityUpdate.setAmount(request.amount());
        return movementEntityUpdate;
    }

    private MovementEntity updateToPatch(MovementRequest request) {
        MovementEntity movementEntityUpdate = new MovementEntity();

        if(request.type() != null && !request.type().isEmpty()){
            movementEntityUpdate.setMovementType(MovementType.fromCode(request.type()));
        }
        if(request.date() != null && !request.date().isEmpty()){
            movementEntityUpdate.setDate(formatDate(request.date()));
        }
        if(request.amount() != null){
            movementEntityUpdate.setAmount(request.amount());
        }

        return movementEntityUpdate;
    }

    private String formatDate(@JsonProperty("date") String dateRequest){
        Date date = new Date(dateRequest);
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    private boolean isAvailableBalance(Double amount, Double openingBalance) {
        if(amount >= 0){
            return true;
        }else{
            return amount <= openingBalance;
        }
    }
}
