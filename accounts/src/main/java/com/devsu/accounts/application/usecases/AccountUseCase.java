package com.devsu.accounts.application.usecases;

import com.devsu.accounts.domain.dto.AccountDto;
import com.devsu.accounts.domain.request.AccountRequest;
import com.devsu.accounts.domain.response.AccountResponse;

import java.util.List;
import java.util.Optional;

public interface AccountUseCase {
    List<AccountDto> findAll();
    Optional<AccountDto> create(AccountRequest request);
    AccountResponse update(AccountRequest request);
    AccountResponse patch(AccountRequest request);
    AccountResponse delete(String id);
    AccountResponse report(String id, String desde, String hasta);
}
