package com.devsu.accounts.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

import java.util.Date;

@Builder
public record MovementRequest (
        @JsonProperty("id") String id,
        @JsonProperty("type") String type,
        @JsonProperty("date") String date,
        @JsonProperty("amount") Double amount
) {}
