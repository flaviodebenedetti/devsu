package com.devsu.accounts.domain.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountResponse {
    private int statusCode;
    private HttpStatus status;
    private String description;

    public static AccountResponse buildOkResponse() {
        return AccountResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .description("OK")
                .build();
    }

    public static AccountResponse buildMessagePublicateResponse() {
        return AccountResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .description("Publish")
                .build();
    }

    public static AccountResponse buildUpdateOkResponse() {
        return AccountResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .description("OK")
                .build();
    }

    public static AccountResponse buildDeleteOkResponse() {
        return AccountResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .description("Registro eliminado con éxito")
                .build();
    }
}
