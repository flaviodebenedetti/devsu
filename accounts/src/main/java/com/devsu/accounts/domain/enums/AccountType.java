package com.devsu.accounts.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum AccountType {
    AHORROS("01", "Ahorros"),
    CORRIENTE("02", "Corriente"),
    ERROR("99", "ERROR");

    private final String code;
    private final String description;

    public static AccountType fromCode(String code) {
        return Arrays.stream(values()).filter(c -> c.getCode().equalsIgnoreCase(code)).findFirst()
                .orElse(ERROR);
    }
}
