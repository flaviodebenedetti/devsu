package com.devsu.accounts.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public record MovementCreateRequest (
        @JsonProperty("type") String type,
        @JsonProperty("date") String date,
        @JsonProperty("balance") Double balance,
        @JsonProperty("amount") Double amount,
        @JsonProperty("account_number") Long accountNumber
) {}
