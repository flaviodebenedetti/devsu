package com.devsu.accounts.domain.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public record AccountRequest (
        @JsonProperty("account_number") Long accountNumber,
        @JsonProperty("account_type") String accountType,
        @JsonProperty("opening_balance") Double openingBalance,
        @JsonProperty("state") Boolean state,
        @JsonProperty("customer_id") Long customerId
) {}
