package com.devsu.accounts.domain.dto;

import com.devsu.accounts.domain.entities.MovementEntity;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDto {
    private Long accountNumber;
    private String accountType;
    private Double openingBalance;
    private boolean state;
    private Long customerId;
    private List<MovementEntity> movementEntities;
}
