package com.devsu.accounts.domain.entities;

import com.devsu.accounts.domain.dto.AccountDto;
import com.devsu.accounts.domain.enums.AccountType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "account")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "account_number")
    private Long accountNumber;
    @Column(name = "account_type")
    private AccountType accountType;
    @Column(name = "opening_balance")
    private Double openingBalance;
    private Boolean state;
    private Long customerId;
    @OneToMany(mappedBy = "account", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.LAZY)
    @JsonIgnore
    private List<MovementEntity> listMovements;

    public AccountDto toAccountDto(){
        return AccountDto.builder()
                    .accountNumber(accountNumber)
                    .accountType(accountType.getDescription())
                    .openingBalance(openingBalance)
                    .state(state)
                    .customerId(customerId)
                    .movementEntities(listMovements)
                    .build();

    }
}
