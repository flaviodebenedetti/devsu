package com.devsu.accounts.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MovementDto {
    private String id;
    private String date;
    private String movement;
    private Double amount;
}
