package com.devsu.accounts.domain.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MovementResponse {

    private int statusCode;
    private HttpStatus status;
    private String description;
    public static final String SUCCESS = "OK";
    public static final String CREATED_SUCCESS = "Creado con éxito";
    public static final String DELETE_SUCCESS = "Registro eliminado con éxito";

    public static MovementResponse buildOkResponse() {
        return MovementResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .description(SUCCESS)
                .build();
    }

    public static MovementResponse buildCreatedResponse() {
        return MovementResponse.builder()
                .status(HttpStatus.CREATED)
                .statusCode(HttpStatus.CREATED.value())
                .description(CREATED_SUCCESS)
                .build();
    }

    public static MovementResponse buildUpdateOkResponse() {
        return MovementResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .description(SUCCESS)
                .build();
    }

    public static MovementResponse buildDeleteOkResponse() {
        return MovementResponse.builder()
                .status(HttpStatus.OK)
                .statusCode(HttpStatus.OK.value())
                .description(DELETE_SUCCESS)
                .build();
    }

}
