package com.devsu.accounts.domain.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum MovementType {
    DEPOSITO("01", "Deposito"),
    RETIRO("02", "Retiro"),
    ERROR("99", "ERROR");

    private final String code;
    private final String description;

    public static MovementType fromCode(String code) {
        return Arrays.stream(values()).filter(c -> c.getCode().equalsIgnoreCase(code)).findFirst()
                .orElse(ERROR);
    }
}
