package com.devsu.accounts.domain.entities;

import com.devsu.accounts.domain.dto.MovementDto;
import com.devsu.accounts.domain.enums.MovementType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import jakarta.persistence.*;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "movement")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class MovementEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.UUID)
    private String id;
    private String date;
    @Column(name = "movement_type")
    private MovementType movementType;
    private Double amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_number")
    @JsonIgnore
    private AccountEntity account;

    public MovementDto toMovementDto() {
        return MovementDto.builder()
                .id(id)
                .date(date)
                .movement(movementType.getDescription())
                .amount(amount)
                .build();
    }

}
