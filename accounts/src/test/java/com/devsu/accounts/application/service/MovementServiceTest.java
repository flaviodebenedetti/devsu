package com.devsu.accounts.application.service;

import com.devsu.accounts.domain.entities.AccountEntity;
import com.devsu.accounts.domain.entities.MovementEntity;
import com.devsu.accounts.domain.enums.MovementType;
import com.devsu.accounts.domain.request.MovementCreateRequest;
import com.devsu.accounts.domain.response.MovementResponse;
import com.devsu.accounts.infrastructure.repository.AccountRepository;
import com.devsu.accounts.infrastructure.repository.MovementRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class MovementServiceTest {

    @InjectMocks
    private MovementService movementService;

    @Mock
    private MovementRepository movementRepository;

    @Mock
    private AccountRepository accountRepository;

    private MovementCreateRequest createRequest;
    private List movementList;
    private AccountEntity account;

    @BeforeEach
    void setup(){
        createRequest = MovementCreateRequest.builder()
                .type("01")
                .date("24/06/1990")
                .value(Double.valueOf(10))
                .balance(Double.valueOf(10)).build();

        MovementEntity entity = MovementEntity.builder()
                .id("test")
                .date("24/06/1990")
                .movementType(MovementType.DEPOSITO)
                .amount(Double.valueOf(100)).build();

        MovementEntity entity2 = MovementEntity.builder()
                .id("test")
                .date("24/06/1990")
                .movementType(MovementType.RETIRO)
                .amount(Double.valueOf(100)).build();

        movementList = new ArrayList<>();
        movementList.add(entity);
        movementList.add(entity2);

        account = AccountEntity.builder()
                .accountNumber(1L)
                .state(true)
                .openingBalance(Double.valueOf(100))
                .customerId(111L)
                .listMovements(movementList)
                .build();
    }

    @Test
    void whenFindAllIsOk(){

        Mockito.when(movementRepository.findAll()).thenReturn(movementList);

        List list= movementService.findAll();

        Assertions.assertEquals(false, list.isEmpty());
    }

    @Test
    void whenCreatelIsOk(){

        Mockito.when(accountRepository.findByAccountNumber(Mockito.any())).thenReturn(account);

        MovementResponse movementResponse = movementService.create(createRequest);

        Assertions.assertEquals(HttpStatus.CREATED, movementResponse.getStatus());
    }
}
